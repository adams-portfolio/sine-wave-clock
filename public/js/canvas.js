import * as dat from 'dat.gui'

var gui = new dat.GUI();
var canvas = document.querySelector('canvas');
var c = canvas.getContext('2d');


canvas.width = innerWidth;
canvas.height = innerHeight;

var wave = {
  y: canvas.height / 2,
  length: 0.01,
  amplitude: 100,
  frequency: 0.009
};

var wave1 = {
  y: canvas.height / 2,
  length: 0.01,
  amplitude: 100,
  frequency: 0.002
};

var wave2 = {
  y: canvas.height / 2,
  length: 0.01,
  amplitude: 100,
  frequency: 0.002
};

var strokeColor = {
  h: 255,
  s: 92,
  l: 31
};

var backgroundColor = {
  r: 0,
  g: 0,
  b: 0,
  a: 0.01
};

var waveFolder = gui.addFolder('waveSeconds');
waveFolder.add(wave, 'y', 0, canvas.height);
waveFolder.add(wave, 'length', -0.01, 0.01);
waveFolder.add(wave, 'amplitude', -300, 300);
waveFolder.add(wave, 'frequency', 0.001, 1);
waveFolder.open();

var waveFolder = gui.addFolder('waveMinutes');
waveFolder.add(wave1, 'y', 0, canvas.height);
waveFolder.add(wave1, 'length', -0.01, 0.01);
waveFolder.add(wave1, 'amplitude', -300, 300);
waveFolder.add(wave1, 'frequency', 0.001, 1);
waveFolder.open();

var waveFolder = gui.addFolder('waveHours');
waveFolder.add(wave2, 'y', 0, canvas.height);
waveFolder.add(wave2, 'length', -0.01, 0.01);
waveFolder.add(wave2, 'amplitude', -300, 300);
waveFolder.add(wave2, 'frequency', 0.001, 1);
waveFolder.open();

var strokeFolder = gui.addFolder('stroke');
strokeFolder.add(strokeColor, 'h', 0, 255);
strokeFolder.add(strokeColor, 's', 0, 100);
strokeFolder.add(strokeColor, 'l', 0, 100);
strokeFolder.open();

var backgroundFolder = gui.addFolder('background');
backgroundFolder.add(backgroundColor, 'r', 0, 255);
backgroundFolder.add(backgroundColor, 'g', 0, 255);
backgroundFolder.add(backgroundColor, 'b', 0, 255);
backgroundFolder.add(backgroundColor, 'a', 0, 1);
backgroundFolder.open();

var increment = wave.frequency;
var increment1 = wave1.frequency;
var increment2 = wave2.frequency;

var amplitude = wave.amplitude
var amplitude1 = wave1.amplitude
var amplitude2 = wave2.amplitude

function animate() {
  requestAnimationFrame(animate);
  c.fillStyle = 'rgba(' + backgroundColor.r + ', ' + backgroundColor.g + ', ' + backgroundColor.b + ', ' + backgroundColor.a + ')';
  c.fillRect(0, 0, canvas.width, canvas.height);

  c.beginPath();
  c.moveTo(0, canvas.width / 2);
  

  for (var i = 0; i < canvas.width; i++) {
    c.lineTo(i, wave.y + Math.sin(i * wave.length + increment) * wave.amplitude * Math.sin(increment));
  }

 for (var i = 0; i < canvas.width; i++) {
    c.lineTo(i, wave1.y + Math.sin(i * wave1.length + increment1) * wave1.amplitude * Math.sin(increment1));
  }
  
  
  for (var i = 0; i < canvas.width; i++) {
    c.lineTo(i, wave2.y + Math.sin(i * wave2.length + increment2) * wave2.amplitude * Math.sin(increment2));
  }

  c.strokeStyle = 'hsl(' + Math.abs(strokeColor.h * Math.sin(increment)) + ', ' + strokeColor.s + '%, ' + strokeColor.l + '%)';
  c.stroke();

  increment += wave.frequency;
  increment1 += wave1.frequency;
  increment2 += wave2.frequency;

  amplitude += wave.amplitude;
  amplitude1 += wave1.amplitude;
  amplitude2 += wave2.amplitude;

}


animate();